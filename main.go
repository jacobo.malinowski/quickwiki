package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
)

func buildUI(app *gtk.Application) {
	window, err := gtk.ApplicationWindowNew(app)
	if err != nil {
		log.Fatal("Unable to create app window:", err)
	}
	window.SetTitle("Search the Wiki")
	window.SetResizable(false)

	box, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 2)
	if err != nil {
		log.Fatal("Unable to create box:", err)
	}
	window.Add(box)

	entry, err := gtk.EntryNew()
	if err != nil {
		log.Fatal("Unable to create entry:", err)
	}
	box.PackStart(entry, false, false, 5)

	button, err := gtk.ButtonNew()
	if err != nil {
		log.Fatal("Unable to create button:", err)
	}
	box.PackStart(button, false, false, 5)

	entry.SetActivatesDefault(true)
	window.SetDefault(button)

	button.Connect("clicked", func() {
		s, _ := entry.GetText()
		fmt.Println(s)
	})
	window.Connect("destroy", func() {
		gtk.MainQuit()
	})

	window.ShowAll()
	gtk.Main()
}

func main() {
	gtk.Init(nil)
	app, err := gtk.ApplicationNew("com.gitlab.jacobo.malinowski.quickwiki", glib.APPLICATION_FLAGS_NONE)
	if err != nil {
		log.Fatal("Unable to application:", err)
	}
	app.Connect("activate", func() {
		buildUI(app)
	})

	app.Run(os.Args)
}
