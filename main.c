#include <stdio.h>
#include <gtk/gtk.h>
// #include <curl/curl.h>

#define BOX widgets[0]
#define ENTRY widgets[1]
#define BUTTON widgets[2]
#define WIKI widgets[3]
#define TEXT widgets[4]

// Creates a tree model containing the entry completion list
GtkTreeModel *create_completion_model(void)
{
  GtkListStore *store;
  GtkTreeIter iter;

  store = gtk_list_store_new (1, G_TYPE_STRING);

  /* Append one word */
  gtk_list_store_append (store, &iter);
  gtk_list_store_set (store, &iter, 0, "GNOME", -1);

  /* Append another word */
  gtk_list_store_append (store, &iter);
  gtk_list_store_set (store, &iter, 0, "total", -1);

  /* And another word */
  gtk_list_store_append (store, &iter);
  gtk_list_store_set (store, &iter, 0, "totally", -1);

  return GTK_TREE_MODEL(store);
}


// performs a search for a given term
int searchForTerm(const char *term)
{

}

// Reads contents of entry and attempts a search
void search(GtkWidget *button, GtkWidget **widgets)
{
	// Searched term
	gchar *term;
	// Entry completion list related pointers
	GtkEntryCompletion *completion;
	GtkTreeModel *completion_model;
	GtkTreeIter *iter;

	// Getting searched term from entry
	term = (char *)gtk_entry_get_text(GTK_ENTRY(ENTRY));

	completion = gtk_entry_get_completion(ENTRY);
	completion_model = gtk_entry_completion_get_model(completion);
	if(!gtk_tree_model_get_iter_first(completion_model, iter)){
		printf("You sent:%s\n", term);
		return;
	}

	// Append new term into the list
  	gtk_list_store_append (GTK_LIST_STORE(completion_model), iter);
  	gtk_list_store_set (GTK_LIST_STORE(completion_model), iter, 0, term, -1);

    gtk_entry_completion_set_model(completion, completion_model);
    //g_object_unref(completion_model);

	gtk_entry_completion_set_text_column(completion, 0);
	return;
	// testing output
	
	// searchForTerm(term);
}

// Displays contents of window
gint firstSearchWindow(GtkWindow *window, GtkWidget **widgets)
{
	// Store keeps all terms searched before
	GtkEntryCompletion *completion;
	GtkTreeModel *completion_model;
	GtkListStore *store;
	GtkTreeIter *iter;

	// Setting up search window
	window = GTK_WINDOW(gtk_window_new(GTK_WINDOW_TOPLEVEL));
	gtk_window_set_title(GTK_WINDOW(window), "Search the Wiki");

	// Setting window size
	//gtk_window_set_default_size(GTK_WINDOW(window),250,100);
	gtk_window_set_resizable(GTK_WINDOW(window), 0);

	// Centering window position
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER_ALWAYS);

	// Creating main window container
	BOX = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 2);
	gtk_container_add(GTK_WINDOW(window), GTK_BOX(BOX));

	// Creating search entry and adding it to the box
	ENTRY = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(BOX), ENTRY, FALSE, FALSE, 5);

	// Creating entry completion object
    completion = gtk_entry_completion_new();

    // Assigning the completion to the entry
    gtk_entry_set_completion(GTK_ENTRY(ENTRY), completion);
    g_object_unref (completion);

    // Creating completion tree model
	store = gtk_list_store_new(1, G_TYPE_STRING);
    completion_model = GTK_TREE_MODEL(store);

    gtk_entry_completion_set_model(completion, completion_model);
    g_object_unref (completion_model);

    // Setting completion model column to 0 as the text column
	// (Tinker with number to see what it does)
    gtk_entry_completion_set_text_column (completion, 0);

	// Creating button and adding it to the box
	BUTTON = gtk_button_new_with_label("Search");
	gtk_box_pack_start(GTK_BOX(BOX), BUTTON, FALSE, FALSE, 5);
	gtk_widget_set_can_default(BUTTON,TRUE);

	// Attempting search if enter is pressed,
	// when entry is active
	gtk_window_set_default(GTK_WINDOW(window), GTK_WIDGET(BUTTON));
	gtk_entry_set_activates_default(GTK_ENTRY(ENTRY), TRUE);

	// Calling search() once the button is clicked
	g_signal_connect(BUTTON, "clicked", (GCallback)search, widgets);
	// Terminating program once the window receives "destroy" signal
	g_signal_connect(window, "destroy", gtk_main_quit, NULL);

	// Setting entire window to be displayed
	gtk_widget_show_all(window);

	// Starting GTK main
	gtk_main();
	// Hiding widgets for now
	gtk_widget_hide(window);

	// Destroying widgets once their job is done
	gtk_widget_destroy(BUTTON);
	gtk_widget_destroy(ENTRY);
	gtk_widget_destroy(BOX);
	gtk_widget_destroy(window);
}

int main(int argc, char **argv)
{
	GtkWindow *window;
	// Array of different widgets
	GtkWidget widgets[5];
	//CURLS *ce;

	// libcURL initialization
	//ce=curl_easy_init();

	// GTK initialization
	gtk_init(&argc, &argv);

	// Search window display
	firstSearchWindow(&window, widgets);
}
