package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	fyne "fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

const (
	wikipediaSearchURL = "https://en.wikipedia.org/w/index.php?search="
// "https://en.wikipedia.org/wiki/"
)

type doer interface {
	Do(req *http.Request) (*http.Response, error)
}

// WikipediaClient is a basic Wikipedia client
type WikipediaClient struct {
	searchURL string

	httpClient doer
}

// NewWikipediaClient default WikipediaClient constructor
func NewWikipediaClient(
	wikiSearchURL string,
	httpClient doer,
) *WikipediaClient {
	return &WikipediaClient{
		searchURL:  wikiSearchURL,
		httpClient: httpClient,
	}
}

// Search enables to do some naive wikipedia search
func (c *WikipediaClient) Search(
	ctx context.Context,
	term string,
) (string, error) {
	cleanTerm := url.PathEscape(strings.TrimSpace(term))
	reqURL := c.searchURL + cleanTerm
	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodGet,
		reqURL,
		nil,
	)
	if err != nil {
		return "", fmt.Errorf("http.NewRequestWithContext: %w", err)
	}

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return "", fmt.Errorf("httpClient.Do: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusNotFound {
		return "", fmt.Errorf("term: %s, was not found", term)
	}
	if resp.StatusCode/100 != 2 {
		return "", fmt.Errorf("bad response code, expected: 2xx, got: %v", resp.StatusCode)
	}

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("io.ReadAll: %w", err)
	}

	return string(data), nil

}

func main() {
	httpClient := &http.Client{
		Transport: &http.Transport{
			Proxy:                 nil,
			TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
			TLSHandshakeTimeout:   2 * time.Second,
			IdleConnTimeout:       10 * time.Second,
			ResponseHeaderTimeout: 2 * time.Second,
			ExpectContinueTimeout: 3 * time.Second,
		},
		Timeout: 4 * time.Second,
	}

	wikiClient := NewWikipediaClient(
		wikipediaSearchURL,
		httpClient,
	)

	search := func(term string) {
		searchCtx, cancelSearchCtx := context.WithTimeout(
			context.Background(),
			time.Second,
		)
		defer cancelSearchCtx()

		b, err := wikiClient.Search(searchCtx, term)
		if err != nil {
			log.Printf("error on term search: %v", err)
		}
		fmt.Println(b)
	}

	app := fyne.New()
	window := app.NewWindow("Search the Wiki")

	entry := widget.NewEntry()
	entry.OnSubmitted = func(s string) {
		search(s)
	}

	grid := container.NewAdaptiveGrid(
		2,
		entry,
		widget.NewButton("Search", func() {
			search(entry.Text)
		}))
	window.SetContent(grid)

	window.ShowAndRun()
}
