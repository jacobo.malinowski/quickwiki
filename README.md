QuickWiki
===========
Desktop application for instant Wiki browsing.

Compiling QuickWiki
===========
+ C version:
    - Run "compile.sh" script to prepare "quickwiki" binary
    - Currently testing entry input completion lists, which could improve Wiki browsing experience in the future
+ Go version:
    - Run "go build main_fyne.go" to prepare fyne binary
+ Rust version:
    - Run "cargo build" to prepare "quickwiki" binary inside "./target/debug/" directory


You might have to set binary file to be executable before you run it. In that case use "chmod +x binaryNameHere"
