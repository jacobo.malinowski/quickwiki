extern crate gtk;

use gio::prelude::*;
use gtk::prelude::*;

use std::env::args;

fn build_ui(application: &gtk::Application) {
    let window = gtk::ApplicationWindow::new(application);
    window.set_title("Search the Wiki");
    window.set_resizable(false);

    let boxy = gtk::Box::new(gtk::Orientation::Horizontal, 2);
    window.add(&boxy);

    let entry = gtk::Entry::new();
    boxy.pack_start(&entry, false, false, 5);
    let button = gtk::Button::with_label("Search");
    boxy.pack_start(&button, false, false, 5);
    
    button.set_can_default(true);
    entry.set_activates_default(true);
    window.set_default(Some(&button));

    let entry_clone = entry.clone();
    button.connect_clicked(move |_| {
        println!("{}", entry_clone.get_text().as_str());
    });
    window.connect_destroy(|window| {
        gtk::main_quit();
    });

    window.show_all();
    gtk::main();

}

fn main() {
    let application =
        gtk::Application::new(Some("com.gitlab.jacobo.malinowski.quickwiki"), Default::default())
            .expect("Initialization failed...");

    application.connect_activate(|app| {
        build_ui(app);
    });

    application.run(&args().collect::<Vec<_>>());
}
